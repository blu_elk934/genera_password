#include<stdio.h>
#include<time.h>
#include<stdlib.h>
#include<ctype.h>



int main(int argc,char **argv){

	srand(time(NULL)); 
		
	int lunghezza_password=0;
	int numeri_caratteri_speciali=0;
	int n_numeri=0;
	int *array_flag;
	int i=0;
	int y;
	int conta=0;
	char intermedio[20];
	char finale[20];
	int numero_casuale;
 	int differenza;
	int j;
	int m;
	int posizione_random=0;
	int uscita;
	int n_caratteri_maiuscoli=0;
	int n_caratteri_minuscoli=0;
	
	/****************chiedo dimensione desiderata tramite linea di comando (per convenzione compreso tra 10 e 20) ed utilizzo rand() per determinare quanti caratteri speciali,quanti numeri e quanti caratteri maiuscoli ******************/
	
	
	
	if(argc-1!=1){
		printf("ERRORE: NON HAI INSERITO NESSUN ARGOMENTO A RIGA DI COMANDO OPPURE HAI INSERITO TROPPI ARGOMENTI!!\n");
	}else{
		lunghezza_password=atoi(argv[1]); //trasformo la stringa acquisita a riga di comando in un numero
	 	if(lunghezza_password>=10 && lunghezza_password<=20){  
		
		
			numeri_caratteri_speciali=(rand()%2)+1; //per convenzione ho deciso che si possono avere da 1 fino ad un max 2 caratteri speciali
			n_numeri=(rand()%2)+1; //per convenzione ho deciso che si possono avere da 1 fino ad un max 2 numeri
			n_caratteri_maiuscoli=(rand()%2)+1; //per convenzione ho deciso che si possono avere da 1 fino ad un max 2 caratteri maiuscoli
			n_caratteri_minuscoli=(lunghezza_password-numeri_caratteri_speciali-n_numeri-n_caratteri_maiuscoli);
			
			
			/***********************alloco array_flag e lo inizializzo a zero (0=libero). L'array terra' traccia se e' gia' stato inserito il carattere in modo randomico nell'array_finale ***/
			array_flag=malloc(lunghezza_password*sizeof(int)); 
			if(array_flag==NULL){
				printf("errore\n");
			}
	
			for(y=0;y<lunghezza_password;y++){
				array_flag[y]=0;
			}
		
	
	
	
	
		
			/********* determino i caratteri speciali (che in esadecimale di ASCII vanno da 33 a 47) utilizzando rand()  e li inserisco in un array temporaneo chiamato intermedio****/
				
			differenza = (47 - 33) + 1; //in ascii 33 a 47 in ascii
		
			while(conta<numeri_caratteri_speciali){ 
				numero_casuale = rand() % differenza ;
 				numero_casuale = numero_casuale + 33 ; 
 				intermedio[i]=(char)numero_casuale;
				i++;
				conta++;			
			}
		
			/********* determino i numeri (che in esadecimale di ASCII vanno da 30 a 39) utilizzando rand()  e li inserisco nell'array temporaneo "intermedio" ****/
			conta=0;
			differenza = (39 - 30) + 1; //da 1 a 9 in ascii
			
			while(conta<n_numeri){
				numero_casuale = rand() % differenza ;
 				intermedio[i]=(char)numero_casuale+'0';
				i++;
				conta++;	
			}
		
	/********* determino i caratteri maiuscoli (che in esadecimale di ASCII vanno da 65 a 90) utilizzando rand() e li inserisco nell'array temporaneo "intermedio".****/
			conta=0;
			differenza = (90 - 65) + 1; //da 65 (A) a 90(Z) in ascii decimale
				
			
			while(conta<n_caratteri_maiuscoli){
				numero_casuale = rand() % differenza;
				numero_casuale = numero_casuale + 65 ;
 				intermedio[i]=(char)numero_casuale;
				i++;
				conta++;	
			}
			
		/********* determino i caratteri minuscoli(che in esadecimale di ASCII vanno da 65 a 90) utilizzando rand() e li inserisco nell'array temporaneo "intermedio"****/
			conta=0;
			differenza = (122 - 97) + 1; //da 97 (a) a 122(z) in ascii decimale
			
			while(conta<n_caratteri_minuscoli){
				numero_casuale = rand() % differenza;
				numero_casuale = numero_casuale + 97;
 				intermedio[i]=(char)numero_casuale;
				i++;
				conta++;	
			}
		
/*** chiamo la funzione rand() che determina la posizione randomica per l'array "finale". Faccio un controllo se nell'array "finale" e' gia' stato inserito qualcosa in precedenza attraverso "array_flag" (0=libera, 1=occupata) e se e' zero allora all'interno di "finale" avro' il valore i-esimo prelevato dall'array "intermedio"; altrimenti richiamo nuovamente rand() fino a quando trovero' una posizione libera ********/
		
			for(m=0;m<i;m++){
				
				
			
				do{
					
					posizione_random=rand()%lunghezza_password;
					uscita = (array_flag[posizione_random]==0); 
					if(uscita){
						finale[posizione_random]=intermedio[m];
						array_flag[posizione_random]=1; 	//marcalo cm occupato
						
					}
				
				}while(!uscita);
			}
			
			/****stampo l'array "finale" con la password generata***/
			
			for(j=0;j<i;j++){
				printf("%c ",finale[j]);
			}
			printf("\n"); 
			
		}else printf("ERRORE: DEVI INSERIRE UN NUMERO COMPRESO TRA 10 E 20\n");	
	}//chiusura primo else
	

return 0;
}
